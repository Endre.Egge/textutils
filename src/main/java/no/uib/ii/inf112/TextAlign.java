package no.uib.ii.inf112;

public class TextAlign implements TextAligner {
    @Override
    public String center(String text, int width) {
        return null;
    }

    @Override
    public String flushRight(String text, int width) {
        return text + "    ";
    }

    @Override
    public String flushLeft(String text, int width) {
        return null;
    }

    @Override
    public String justify(String text, int width) {
        return null;
    }
}
